let mapleader = " "

" increase/decrease terminal size
nnoremap <C-Up> :res +1<CR>
nnoremap <C-Down> :res -1<CR>

" open terminal, find python virtual environments and activate first one, if present
nnoremap <leader>t :below terminal<CR><C-W>:res -10<CR>VENV=$(ls -l */bin/python 2>/dev/null \| head -n1 \| awk -F "/" '{print $1}'); if [ ! -z "$VENV" ]; then source $VENV/bin/activate; fi; clear<CR>

" ctrl-P
nnoremap <Ctrl-P> :CtrlP<CR>

" nerdtree
nnoremap <leader>e :NERDTreeToggle<CR>

" YCM
nnoremap gd :YcmCompleter GoTo<CR>
nnoremap gr :YcmCompleter GoToReferences<CR>
nnoremap gt :YcmCompleter GetType<CR>
