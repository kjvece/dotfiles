augroup NERD
    autocmd!

    " autoopen nerdtree when no files given
    autocmd StdinReadPre * let s:std_in=1
    autocmd VimEnter * if exists("NERDTree") | NERDTree | if argc() > 0 || exists("s:std_in") | wincmd p | endif | endif
    autocmd BufEnter * if tabpagenr("$") == 1 && winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree() | quit | endif

    " autoclose nerdtree when it is the last buffer
    autocmd BufEnter * if winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree() | quit | endif
augroup END
