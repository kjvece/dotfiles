" show popups and dont use preview window
set completeopt=menu,popup,longest
let g:ycm_autoclose_preview_window_after_insertion = 1
