let g:ctrlp_custom_ignore = {
 \      'dir': '\v(\.git|env|\.egg-info|__pycache__|node_modules|dist|target)$',
 \      'file': '\v\.(exe|so|dll|pyc|swp|egg-info)',
 \  }
