let s:autoload = '~/.vim/autoload/'
if empty(glob(s:autoload.'plug.vim'))
    silent execute '!mkdir -p '.s:autoload.' && wget https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim -O '.s:autoload.'plug.vim'
    augroup VIMPLUG
        autocmd!
        autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
    augroup END
endif

call plug#begin()
    Plug 'NLKNguyen/papercolor-theme'

    Plug 'ctrlpvim/ctrlp.vim'

    Plug 'preservim/nerdtree'
    Plug 'Xuyuanp/nerdtree-git-plugin'
    Plug 'ryanoasis/vim-devicons'
    Plug 'scrooloose/nerdtree-project-plugin'

    Plug 'vim-airline/vim-airline'

    Plug 'tpope/vim-fugitive'
    Plug 'jreybert/vimagit'
    Plug 'airblade/vim-gitgutter'
    Plug 'airblade/vim-rooter'

    Plug 'ycm-core/YouCompleteMe', { 'do': './install.py --clang-completer --ts-completer' }
call plug#end()

let s:path = "$HOME/.vim/config/plugins/"
let s:plugins = [
 \      "ctrlp.vim",
 \      "nerdtree.vim",
 \      "ycm.vim",
 \  ]

for plugin in s:plugins
    try
        execute "source " s:path . plugin
    catch
        echom "Error loading plugin: ". v:exception
    endtry
endfor
