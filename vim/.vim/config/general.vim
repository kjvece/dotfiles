" tabs and indentation
set shiftwidth=4
set tabstop=4
set softtabstop=4
set expandtab
set smartindent

" display, colors, and theming
set list
set listchars=tab:»»,trail:·
set cursorline
set laststatus=2
set number
set relativenumber
set background=dark
try
    colorscheme PaperColor
catch
    colorscheme slate
endtry

" buffers
set hidden
set updatetime=200
set splitright
set splitbelow

" files
set noswapfile
set undodir=~/.vim/undo//
set undofile

" remove numbering from terminal
augroup TERMINAL
    autocmd!
    autocmd TerminalOpen * set nonumber norelativenumber
augroup END

