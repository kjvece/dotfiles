#!/bin/bash

packages="stow vim python3 python3-venv nodejs"

if [ ! -z $(which apt) ]; then
    sudo apt install $packages -y
elif [ ! -z $(which dnf) ]; then
    sudo dnf install $packages -y
fi

configs="$(ls -d */)"
for config in $configs; do
    stow ${config::-1}
done
