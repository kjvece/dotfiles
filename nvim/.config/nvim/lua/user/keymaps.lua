vim.g.mapleader = " "

local keymap = vim.keymap

local opts = {
	noremap = true,
	silent = true,
}

keymap.set("t", "<ESC>", "<C-\\><C-N>", opts)
keymap.set("n", "<leader>st", "<cmd>split<CR><cmd>terminal<CR>", opts)

keymap.set("n", "<leader>nh", "<cmd>noh<CR>", opts)

keymap.set("n", "<leader>sv", "<cmd>vertical split<CR>", opts)
keymap.set("n", "<leader>sh", "<cmd>split<CR>", opts)
keymap.set("n", "<leader>sc", "<cmd>q<CR>", opts)
keymap.set("n", "<C-h>", "<C-W>h", opts)
keymap.set("n", "<C-j>", "<C-W>j", opts)
keymap.set("n", "<C-k>", "<C-W>k", opts)
keymap.set("n", "<C-l>", "<C-W>l", opts)

keymap.set("n", "<leader>sm", ":MaximizerToggle<CR>", opts)
