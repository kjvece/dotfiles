local opt = vim.opt

opt.hidden = true

opt.number = true
opt.relativenumber = true

opt.tabstop = 2
opt.softtabstop = 2
opt.shiftwidth = 2
opt.expandtab = true
opt.autoindent = true

opt.ignorecase = true
opt.smartcase = true

opt.cursorline = true

opt.background = "dark"
opt.signcolumn = "yes"

opt.splitright = true
opt.splitbelow = true

opt.iskeyword:append("-")

vim.cmd([[
set list
set listchars=tab:»»,trail:·
]])

vim.cmd([[
augroup TERMINAL
    autocmd!
    autocmd TermOpen * set nonumber norelativenumber
augroup END
]])
