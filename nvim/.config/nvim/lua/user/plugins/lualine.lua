local status, lualine = pcall(require, 'lualine')
if not status then
  return
end

local theme = require 'lualine.themes.ayu_dark'

lualine.setup({
  options = {
    theme = theme,
  },
})
