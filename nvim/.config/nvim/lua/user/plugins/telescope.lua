local status, telescope = pcall(require, 'telescope')
if not status then
  return
end

local actions = require 'telescope.actions'

telescope.setup({
})

telescope.load_extension('fzf')

local keymap = vim.keymap

local opt = {
  noremap = true,
  silent = true,
}

keymap.set('n', '<C-P>', '<cmd>Telescope find_files<cr>', opt)
keymap.set('n', '<leader>ff', '<cmd>Telescope find_files<cr>', opt)
keymap.set('n', '<leader>fs', '<cmd>Telescope live_grep<cr>', opt)
keymap.set('n', '<leader>fc', '<cmd>Telescope grep_string<cr>', opt)
keymap.set('n', '<leader>fb', '<cmd>Telescope buffers<cr>', opt)
keymap.set('n', '<leader>fh', '<cmd>Telescope help_tags<cr>', opt)

