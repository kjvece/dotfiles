local status, lspsaga = pcall(require, 'lspsaga')
if not status then
  return
end

lspsaga.init_lsp_saga({
  finder_action_keys = {
    open = '<CR>',
  },
  definition_action_keys = {
    edit = '<CR>',
  },
})
