local status, lspconfig = pcall(require, "lspconfig")
if not status then
	return
end

local cstatus, cmp_nvim_lsp = pcall(require, "cmp_nvim_lsp")
if not cstatus then
	return
end

local on_attach = function(_, bufnr)
	local opts = { noremap = true, silent = true, buffer = bufnr }
	local keymap = vim.keymap

	keymap.set("n", "gf", "<cmd>Lspsaga lsp_finder<CR>", opts)
	keymap.set("n", "gD", "<cmd>lua vim.lsp.buf.declaration()<CR>", opts)
	keymap.set("n", "gd", "<cmd>Lspsaga peek_definition<CR>", opts)
	keymap.set("n", "gi", "<cmd>lua vim.lsp.buf.implementation()<CR>", opts)
	keymap.set("n", "<leader>la", "<cmd>Lspsaga code_action<CR>", opts)
	keymap.set("n", "<leader>lr", "<cmd>Lspsaga rename<CR>", opts)
	keymap.set("n", "<leader>ld", "<cmd>Lspsaga show_line_diagnostics<CR>", opts)
	keymap.set("n", "<leader>ld", "<cmd>Lspsaga show_cursor_diagnostics<CR>", opts)
	keymap.set("n", "K", "<cmd>Lspsaga hover_doc<CR>", opts)
end

local capabilities = cmp_nvim_lsp.default_capabilities()

local servers = {
	"rust_analyzer",
	"clangd",
	"pyright",
	"tsserver",
}

for _, server in pairs(servers) do
	lspconfig[server].setup({
		capabilities = capabilities,
		on_attach = on_attach,
	})
end

lspconfig["sumneko_lua"].setup({
	capabilities = capabilities,
	on_attach,
	settings = {
		Lua = {
			diagnostics = {
				globals = { "vim" },
			},
			workspace = {
				library = {
					[vim.fn.expand("$VIMRUNTIME/lua")] = true,
					[vim.fn.stdpath("config") .. "/lua"] = true,
				},
			},
		},
	},
})
