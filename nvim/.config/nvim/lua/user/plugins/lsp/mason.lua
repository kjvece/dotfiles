local status, mason = pcall(require, 'mason')
if not status then
  return
end

local lstatus, mason_lspconfig = pcall(require, 'mason-lspconfig')
if not lstatus then
  return
end

local nstatus, mason_null_ls = pcall(require, 'mason-null-ls')
if not nstatus then
  return
end

mason.setup()
mason_lspconfig.setup()
mason_null_ls.setup({
  automatic_setup = true,
})
