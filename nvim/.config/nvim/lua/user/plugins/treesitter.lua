local status, treesitter = pcall(require, "treesitter")
if not status then
	return
end

treesitter.setup({
	highlight = {
		enable = true,
	},
	indent = {
		enable = true,
	},
	ensure_installed = {
		"c",
		"cpp",
		"dart",
		"go",
		"rust",
		"python",
		"vim",
		"javascript",
		"typescrypt",
		"tsx",
		"html",
		"css",
		"json",
		"yaml",
		"toml",
		"regex",
		"markdown",
		"bash",
		"lua",
		"dockerfile",
		"gitignore",
	},
})
